import Vue from 'vue'
import Vuex, { Store } from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        danhSachGD: [
            {TenGiaoDich : "Mua nhà", LoaiGD : "Chuyển khoản", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Sacombank"},
            {TenGiaoDich : "Mua Tivi", LoaiGD : "Chuyển khoản", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Vietcombank"},
            {TenGiaoDich : "Rút tiền", LoaiGD : "Tiền mặt", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Agribank"},
            {TenGiaoDich : "Rút tiền", LoaiGD : "Tiền mặt", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Vietcombank"},
            {TenGiaoDich : "Rút tiền", LoaiGD : "Tiền mặt", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Viettinbank"},
            {TenGiaoDich : "Mua nhà", LoaiGD : "Chuyển khoản", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Sacombank"},
            {TenGiaoDich : "Mua nhà", LoaiGD : "Chuyển khoản", NgayGD : "12/08/2018", SoTien : 20000, DonVi : "VNĐ", NganHang : "Agribank"}
        ],
        danhSachNganHang: ['Sacombank','Vietcombank','Viettinbank','Agribank'],
        nganHangDuocChon: 'Sacombank',
        username: 'admin',
        password: 'admin'
    },
    mutations: {
        chonNganHang (state, nganHang) {
          state.nganHangDuocChon = nganHang
        }
    },
    actions: {
        chonNganHang (context, nganHang) {
          context.commit('chonNganHang',nganHang)
        }
    },
    getters: {
        layDSTheoNganHang: state => {
          return state.danhSachGD.filter(giaoDich => giaoDich.NganHang == state.nganHangDuocChon)
        }
    }
})