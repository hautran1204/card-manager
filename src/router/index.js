import Vue from 'vue'
import Router from 'vue-router'
import Admin from '@/components/Admin'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/index',
      name: 'Admin',
      component: Admin
    }
  ]
})
